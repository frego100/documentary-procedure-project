
from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.views import redirect_to_login
from django.core.exceptions import ImproperlyConfigured, PermissionDenied
from django.contrib.auth.mixins import AccessMixin
from functools import wraps
from urllib.parse import urlparse
from django.shortcuts import resolve_url
from django.contrib.auth.decorators import user_passes_test

class RolPermisosMixin(AccessMixin):
    rol_permission_required = None

    def get_permission_required(self):
        if self.rol_permission_required is None:
            raise ImproperlyConfigured(
                '{0} is missing the rol_permission_required attribute. Define {0}.rol_permission_required, or override '
                '{0}.get_permission_required().'.format(self.__class__.__name__)
            )
        if isinstance(self.rol_permission_required, str):
            perms = (self.rol_permission_required,)
        else:
            perms = self.rol_permission_required
        return perms

    def has_permission(self):
        perms = self.get_permission_required()
        return self.request.user.rol in perms
        # return True

    def dispatch(self, request, *args, **kwargs):
        if not self.has_permission():
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


def rol_permission_required(perm, login_url=None, raise_exception=False):
    """
    Decorator for views that checks whether a user has a particular permission
    enabled, redirecting to the log-in page if necessary.
    If the raise_exception parameter is given the PermissionDenied exception
    is raised.
    """
    def check_perms(user):
        if isinstance(perm, str):
            perms = (perm,)
        else:
            perms = perm
        # First check if the user has the permission (even anon users)
        if user.rol in perms:
            return True
        # In case the 403 handler should be called raise the exception
        if raise_exception:
            raise PermissionDenied
        # As the last resort, show the login form
        return False
    return user_passes_test(check_perms, login_url=login_url)