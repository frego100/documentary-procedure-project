import requests
import json

with open( 'bienestar/api.json', 'r') as file:
    config = json.load(file)

URL=config['ruta']['URL']

def generar_ruta(parametros):
    return f"{URL}{parametros}"

def realizar_consulta( url, method, parametro):
    if method =='token':
        query = requests.post(url,parametro)

    else:
        query = requests.get(url, headers =parametro)
    consulta = query.json()
    return consulta

def consulta(url, parametros):

    header = token()
    api = f"{url}?"
    contador=0
    for clave, valor in parametros.items():
        if contador ==(len(parametros.items())-1):
            api+=f"{clave}={valor}"
        else:
            api+=f"{clave}={valor}&"
        contador+=1
    
    consulta = realizar_consulta(api,'get', header)
    return consulta

def token():
    """
    Metodo para obtener el token de autentificacion con le api externo
    """
    # direccion del login para el api
    # agregacion de campos para la autentificacion
    url =generar_ruta(config['auth']['URL'])
    data=config['auth']['data']
    consulta =realizar_consulta(url, 'token', data)
    # lectura del token
    tok = consulta['access_token']
    # agregando bearer autentication
    token = f"Bearer {tok}"
    header = {
        'Authorization': token
    } 
    return header

def beneficiarios(page,tipo):
    """
    Metodo para obtener todos los alumnos dentro del api que seran beneficiarios 

    Parámetros:
    page -- numero de pagina dentro de ruta alumno de la api a consultar
    """
    url =generar_ruta(config[tipo]['ruta'])

    parametros={
        "_page": page,
        "_limit": 20
    }
    response = consulta(url, parametros)
    return response

def obtenerBeneficiarios(apellido,nombre,tipo):
    """
    Metodo para obtener todos los alumnos dentro del api que seran beneficiarios 

    Parámetros:
    apellido -- apellidos completos del alumno a buscar
    nombre --  indicios de nombres del alumno a buscar
    """
    url =generar_ruta(tipo)

    apellido=apellido.replace(" ", "/")

    parametros={
        "APELLIDOS_Y_NOMBRES_like": f"{apellido}, {nombre}"
    }
    response = consulta(url,parametros)
    return response

# def obtenerBeneficiarioDNI(dni):
#     """
#     Metodo para obtener todos los alumnos segun dni

#     Parámetros:
#     dni -- indicio del DNI del alumno para consultar en la api
#     """
#     url = f"https://api-alumno-unsa.herokuapp.com/alumnos?DNI={dni}"
#     header = token()
#     request = requests.get(url, headers =header)
#     return request.json()

# def obtenerBeneficiarioCUI(cui):
#     """
#     Metodo para obtener todos los alumnos segun CUI 

#     Parámetros:
#     cui -- indicio del CUI del alumno para consultar en la api
#     """
#     url = f"https://api-alumno-unsa.herokuapp.com/alumnos?CUI={cui}"
#     header = token()
#     request = requests.get(url, headers =header)
#     return request.json()
    
def obtenerBeneficiarioNombre(nombre,tipo):
    """
    Metodo para obtener todos los alumnos segun sus nombres

    Parámetros:
    nombre -- indicio del nombre para consultar en la api
    """
    url =generar_ruta(tipo)
    parametros={
        "APELLIDOS_Y_NOMBRES_like": nombre
    }
    response = consulta(url,parametros)
    return response

def obtenerBeneficiarioApellido(apellido,tipo):
    """
    Metodo para obtener todos los alumnos segun apellido

    Parámetros:
    apellido -- indicio de apellido, si ingresa 2 apellidos, se reemplazara los espacios por '/' para segun nomenclatura de la api
    """
    url =generar_ruta(tipo)
    apellido=apellido.replace(" ", "/")
    parametros={
        "APELLIDOS_Y_NOMBRES_like": f"{apellido}"
    }
    response = consulta(url,parametros)
    return response
    
def apoyoPsicopedagogico():
    url = generar_ruta('apoyo_psicopedagogico')
    parametros={}
    response = consulta(url,parametros)
    return response

def obtenerSolicitudApoyoDNI(dni):
    url = generar_ruta('apoyo_psicopedagogico')
    parametros={
        'dre_dni_like':dni
        }

    response = consulta(url,parametros)
    return response


def obtenerSolicitudApoyoCUI(cui):
    url = generar_ruta('apoyo_psicopedagogico')
    parametros={
        'dre_cui_like':cui
        }

    response = consulta(url,parametros)
    return response
