"""
Modelos usados para el sistema de trámite documentario.

El diagrama de la base de datos se puede encontrar en el 
siguiente link: https://app.lucidchart.com/invitations/accept/b1ad9ddb-c7b3-4bc1-9b22-cb5aa67b99cf
"""

from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager, PermissionsMixin
from django.urls import reverse
from gdstorage.storage import GoogleDriveStorage

from furl import furl
import time

gd_storage = GoogleDriveStorage()

#metodos para obtener la ruta a la cual se subira los archivos, seran asi: CARPETA/apellidos, nombres (email)/archivo
CARPETA_COMPARTIDA = 'Archivos-Software Trámite Documentario/Convocatoria Comedor Universitario' #este es el nombre de la carpeta que se comparte con la cuenta de servicio del proyecto (https://developers.google.com/identity/protocols/oauth2/service-account)
def obtener_path_documento_expediente(instance, filename):
    return f'{CARPETA_COMPARTIDA}/{instance.expediente.usuario_beneficiario.apellidos}, {instance.expediente.usuario_beneficiario.nombres} ({instance.expediente.usuario_beneficiario.email})/{time.time()}{filename}'

def obtener_path_documento(instance, filename):
    return f'{CARPETA_COMPARTIDA}/{instance.usuario.apellidos}, {instance.usuario.nombres} ({instance.usuario.email})/{time.time()}{filename}'


class Area(models.Model):
    nombre = models.CharField(max_length=30)

    def __str__(self):
        return self.nombre


class Entidad(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField(null=True, blank=True)
    I = 'I'
    S = 'S'
    B = 'B'
    N = 'N'
    C = 'C'
    AREAS_POSIBLES = [
        (I, "Ingenieria"),
        (S, "Sociales"),
        (B, "Biomedicas"),
        (N, "Independiente"),
        (C, "Central")
    ]
    area = models.CharField(max_length=1, choices=AREAS_POSIBLES, default=N)

    def __str__(self):
        return self.nombre


class Beneficiario(models.Model):
    nombres = models.CharField(max_length=30)
    apellidos = models.CharField(max_length=30, null=True, blank=True)
    identificador = models.CharField(max_length=8, null=True, blank=True)
    is_interno = models.BooleanField(null=True, blank=True)

    DP = 'DE'
    SC = 'ES'
    SE = 'SE'
    ENTIDAD_POSIBLES = [
        (DP, "Dependencia"),
        (SC, "Escuela"),
        (SE, "Sector")
    ]
    tipo_entidad = models.CharField(
        max_length=2, choices=ENTIDAD_POSIBLES, default=SE)

    AL = 'AL'
    PR = 'PR'
    AD = 'AD'
    EX = 'EX'
    TIPOS_POSIBLES = [
        (AL, "Alumno"),
        (PR, "Profesor"),
        (AD, "Administrativo"),
        (EX, "Externo")
    ]
    tipo_rol = models.CharField(
        max_length=2, choices=TIPOS_POSIBLES, default=EX)
    entidad = models.ForeignKey(Entidad, on_delete=models.CASCADE)

    def __str__(self):
        return "{} {}".format(self.nombres, self.apellidos)


class Actividad(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre


class SubActividad(models.Model):
    nombre = models.CharField(max_length=250, null=True, blank=True)
    actividad = models.ForeignKey(
        Actividad, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return "{} - {}".format(self.nombre, self.actividad)


class UserManager(BaseUserManager):

    def create_user(self, email, password, *args, **kwargs):
        model = self.model(
            email=self.normalize_email(email),
            rol=kwargs['rol'],
            oficina=kwargs['oficina']
        )
        model.set_password(password)
        model.save(using=self._db)
        return model

    def create_superuser(self, email, password, *args, **kwargs):
        model = self.model(
            email=self.normalize_email(email),
            rol=kwargs['rol'],
            oficina=kwargs['oficina']
        )
        model.is_staff = True
        model.is_admin = True
        model.is_superuser = True
        model.set_password(password)
        model.save(using=self._db)
        return model


class User(AbstractUser):
    """
    El usuario de la Subdirección de Bienestar Universitario
    o el solicitante.
    """
    nombres = models.CharField(max_length=60, blank=True)
    apellidos = models.CharField(max_length=60, blank=True)
    username = models.CharField(max_length=60, blank=True)
    email = models.EmailField(unique=True)
    
    google_profile_picture = models.URLField(max_length=200, default='', blank=True, null=True)

    AS = 'AS'
    SO = 'SO'
    SG = 'SG'
    SB = 'SB'
    JO = 'JO'
    TS = 'TS'
    BE = 'BE'
    ROLES_POSIBLES = [
        (AS, "Administrador general del sistema"),
        (SO, "Secretaria de oficina"),
        (SG, "Secretaria general"),
        (SB, "Jefe Sub-Dirección Bienestar"),
        (JO, "Jefe Oficina"),
        (TS, "Trabajador social"),
        (BE, "Solicitantes")
    ]
    rol = models.CharField(max_length=2, choices=ROLES_POSIBLES, default=TS)
    CU = 'CU'
    AI = 'AI'
    CJ = 'CJ'
    TR = 'TR'
    DM = 'DM'
    OFICINAS_POSIBLES = [
        #(CU, "Comedor Universitario"), #las otras oficinas estan ocultas porque aun no se han implementado
        (AI, "Ayuda Integral"),
        #(CJ, "Cuna Jardin"),
        #(TR, "Transporte"),
        #(DM, "Departamento Medico")
    ]
    oficina = models.CharField(
        max_length=2, choices=OFICINAS_POSIBLES, default=AI)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['rol', 'oficina']

    beneficiario = models.OneToOneField(
        Beneficiario, on_delete=models.CASCADE, null=True, blank=True)
    objects = UserManager()
    def __str__(self):
        return self.nombres


class Expediente(models.Model):
    fecha_recepcion = models.DateField()
    numero_expediente = models.CharField(max_length=30, unique=True)
    descripcion = models.TextField(null=True, blank=True)
    numero_proveido = models.CharField(max_length=30, blank=True)
    numero_oficio = models.CharField(max_length=30, blank=True)
    fecha_salida = models.DateField(null=True, blank=True)

    #dato para llenar el numero de expediente de entrada por solicitud
    n_expediente_solicitud = models.IntegerField(unique=True, null=True)

    #datos para llenar los correlativos de las salidas
    n_oficio = models.IntegerField(unique=True, null=True)
    n_proveido = models.IntegerField(unique=True, null=True)
    n_expediente = models.IntegerField(unique=True, null=True)
    n_informe = models.IntegerField(unique=True, null=True)

    #correlativos de salida
    salida_oficio = models.CharField(max_length=50, blank=True, null=True)
    salida_proveido = models.CharField(max_length=50, blank=True, null=True)
    salida_expediente = models.CharField(max_length=50, blank=True, null=True)
    salida_informe = models.CharField(max_length=50, blank=True, null=True)


    PE = 'PE'
    PR = 'PR'
    RE = 'RE'
    CO = 'CO'
    ESTADOS_POSIBLES = [(PE, "Pendiente"), (PR, "Proceso"),
                        (RE, "Revision"), (CO, "CONCLUIDO")]
    estado_expediente = models.CharField(
        max_length=2, choices=ESTADOS_POSIBLES, default=PE)
    usuario_beneficiario = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True,blank = True)
    beneficiario = models.ForeignKey(
        Beneficiario, on_delete=models.CASCADE, null=True,blank = True)
    actividad = models.ForeignKey(
        Actividad, on_delete=models.CASCADE, null=True)
    sub_actividad = models.ForeignKey(
        SubActividad, on_delete=models.CASCADE, null=True, blank=True)
    objects = models.Manager()
    PR = 'PR'
    PC = 'PC'
    IM = 'IM'
    RESULTADOS_POSIBLES = [(PR, "Proceso"), (PC, "Procedente"),
                        (IM, "Improcedente")]
    resultado = models.CharField(
        max_length=2, choices=RESULTADOS_POSIBLES, default=PR)
    def __str__(self):
        return "{} - {}".format(self.numero_expediente, self.beneficiario)


class Solicitud(models.Model):
    motivo = models.TextField()
    # beneficiario = models.OneToOneField(Beneficiario, on_delete=models.CASCADE, null=True, blank=True)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)

    AP = 'AP'
    DE = 'DE'
    PR = 'PR'
    problema = models.CharField(max_length=60)
    ESTADOS_POSIBLES = [(AP, "procedente "),
                        (DE, "improcedente"), (PR, "proceso")]
    estado_solicitud = models.CharField(
        max_length=2, choices=ESTADOS_POSIBLES, default=PR)
    expediente = models.OneToOneField(
        Expediente, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return "{} - {}".format(self.problema, self.usuario)
    
    @property
    def get_derivacion_trabajador_social(self):
        return UsuarioDerivacion.objects.get(usuario_derivado__rol=User.TS, expediente=self.expediente)


class UsuarioDerivacion(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    usuario_derivado = models.ForeignKey(
        User, on_delete=models.SET_NULL, related_name='usuario_derivado', null=True)
    expediente = models.ForeignKey(
        Expediente, on_delete=models.CASCADE, related_name='derivados')
    fecha = models.DateTimeField(auto_now_add=True)
    observacion = models.TextField(null=True, blank=True)

    def __str__(self):
        return "{} - {} - {}".format(self.usuario, self.expediente, self.usuario_derivado)


class HistorialDeExpediente(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    expediente = models.ForeignKey(
        Expediente, on_delete=models.CASCADE, related_name='historialDeCambios')
    fecha = models.DateTimeField(auto_now_add=True)
    titulo = models.TextField(null=True, blank=True)
    descripcion = models.TextField(null=True, blank=True)

    def __str__(self):
        return "{}".format(self.descripcion)


class TipoDocumento(models.Model):
    nombre = models.CharField(max_length=60)
    descripcion = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.nombre


class Documento(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=60)
    path = models.FileField(
        upload_to=obtener_path_documento, storage=gd_storage, max_length=400)
    
    @property
    def path_url_embed(self):
        urlDefault = 'https://drive.google.com/file/d/ID_DOCUMENTO/preview'
        idDoc = furl(self.path.url).args['id']
        return urlDefault.replace('ID_DOCUMENTO', idDoc)


class DocumentoExpediente(models.Model):
    expediente = models.ForeignKey(Expediente, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=40, null=True, blank=True)
    path = models.FileField(
         upload_to=obtener_path_documento_expediente, null=True, blank=True, storage=gd_storage, max_length=400)

    def __str__(self):
        return self.nombre

    @property
    def path_url_embed(self):
        urlDefault = 'https://drive.google.com/file/d/ID_DOCUMENTO/preview'
        idDoc = furl(self.path.url).args['id']
        return urlDefault.replace('ID_DOCUMENTO', idDoc)


class UsuarioMiembro(models.Model):
    '''Usuario perteneciente a la suboficina de bienestar.

    Usado para dinámicamente dar un rol y oficina a los trabajadores
    registrados.
    '''
    correo = models.EmailField(
        unique=True
    )
    oficina = models.CharField(
        max_length=2,
        choices=User.OFICINAS_POSIBLES,
    )
    rol = models.CharField(
        max_length=2,
        choices=User.ROLES_POSIBLES,
    )

    def __str__(self):
        return self.correo

class FolderDrive(models.Model):
    url = models.TextField()
    idDrive = models.TextField()
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    
    def __str__(self):
        return "id:" + self.idDrive

class ArchivoDrive(models.Model):
    nombre = models.TextField()
    url = models.TextField()
    idDrive = models.TextField()
    folder = models.ForeignKey(FolderDrive, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.nombre

class FolderDrive2(models.Model):
    url = models.TextField()
    idDrive = models.TextField()
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    
    def __str__(self):
        return "id:" + self.idDrive

class ArchivoDrive2(models.Model):
    nombre = models.TextField()
    url = models.TextField()
    idDrive = models.TextField()
    folder = models.ForeignKey(FolderDrive2, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.nombre

