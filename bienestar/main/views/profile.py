from django.contrib.auth.decorators import login_required
from main.mixins import rol_permission_required
from django.shortcuts import render


@login_required()
@rol_permission_required(('AS', 'JO', 'TS', 'SG', 'SB', 'SO','BE'))
def profile_detail_view(request):
    '''Vista que muestra la información del usuario actual'''
    
    return render(
        request,
        template_name='main/profile.html',
        context={'user': request.user}
    )
