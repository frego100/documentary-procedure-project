from django.shortcuts import render
from .forms import UploadDocumentForm, UploadFileForm
from django.http import HttpResponseRedirect
from main.mixins import RolPermisosMixin,rol_permission_required
from django.contrib.auth.decorators import login_required
# Imaginary function to handle an uploaded file.
# from somewhere import handle_uploaded_file

@login_required()
@rol_permission_required(('AS','JO','TS','SG','SB'))
def upload_doc(request):
    form = UploadDocumentForm()
    if request.method == 'POST':
        form = UploadDocumentForm(request.POST, request.FILES)  # Do not forget to add: request.FILES
        if form.is_valid():
            # Do something with our files or simply save them
            # if saved, our files would be located in media/ folder under the project's base folder
            model=form 
            form.save()
            print("hola")
    return render(request, 'main/update.html', locals())


@login_required()
@rol_permission_required(('AS','JO','TS','SG','SB'))
def upload_file(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            handle_uploaded_file(request.FILES['file'])
            return HttpResponseRedirect('/success/url/')
    else:
        form = UploadFileForm()
    return render(request, 'upload.html', {'form': form})

def handle_uploaded_file(f):
    with open('some/file/name.txt', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)