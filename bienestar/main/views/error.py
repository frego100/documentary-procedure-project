from django.shortcuts import render

# Create your views here.


from django.http import HttpResponse

def error_404_view(request, exception):
    return render(request,'error-404.html')

def error_500_view(request):
    return render(request,'error-500.html')

def error_403_view(request, exception):
    return render(request,'error-403.html')