from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from main.models import User

class HomeView(TemplateView):
    template_name='main/home.html'

@login_required()
def dispatch_view(request):
    user = request.user
    if user.rol == User.BE:
        return redirect(reverse('ayuda_integral:solicitud-list'))
    else:
        return redirect(reverse('ayuda_integral:expediente-list'))