from django.dispatch import receiver
from allauth.account import signals
from allauth.socialaccount.models import SocialLogin
from main.models import User, Documento, DocumentoExpediente
from django.db.models.signals import post_delete


@receiver(signals.user_signed_up)
def populate_profile(sociallogin: SocialLogin, user: User, **kwargs):
    if sociallogin.account.provider == 'google':
        user_data = user.socialaccount_set.get(provider='google').extra_data
        user.google_profile_picture = user_data.get('picture', '')
        user.nombres = user_data.get('given_name', '').title()
        user.apellidos = user_data.get('family_name', '').title()
        user.save()

@receiver(post_delete, sender=DocumentoExpediente)
def submission_delete_doc_exp(sender, instance, **kwargs):
    instance.path.delete(False)

@receiver(post_delete, sender=Documento)
def submission_delete_doc(sender, instance, **kwargs):
    instance.path.delete(False)
