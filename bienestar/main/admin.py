from django.contrib import admin
from . import models
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
# from django.contrib.auth.models import Permission

class UserAdmin(BaseUserAdmin):
    list_display = ('email',)
    add_fieldsets = (
        (None,{
            'fields': ('email','oficina','rol','password1','password2')
        })
    ,)
    fieldsets = (
        (None,{
            'fields': ('email','oficina','rol', 'google_profile_picture')
        })
    ,)

admin.site.register(models.User,UserAdmin)
admin.site.register(models.Expediente)
admin.site.register(models.UsuarioDerivacion)
admin.site.register(models.Beneficiario)
admin.site.register(models.Actividad)
admin.site.register(models.Entidad)
admin.site.register(models.Documento)
admin.site.register(models.TipoDocumento)
admin.site.register(models.DocumentoExpediente)
admin.site.register(models.Solicitud)
admin.site.register(models.UsuarioMiembro)
admin.site.register(models.SubActividad)
admin.site.register(models.HistorialDeExpediente)
# admin.site.register(Permission)


# admin.site.register(models.Solicitud)


# Register your models here.
