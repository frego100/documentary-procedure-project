from typing import Dict
from allauth.socialaccount.models import SocialLogin
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from main.models import (
    UsuarioMiembro, User
)


class CustomSocialAccountAdapter(DefaultSocialAccountAdapter):
    def populate_user(self, request,
                      sociallogin: SocialLogin,
                      user_data: Dict[str, str]) -> User:
        user: User = super().populate_user(
            request, sociallogin, user_data
        )

        if UsuarioMiembro.objects.filter(
                correo=user.email).exists():
            info_miembro = UsuarioMiembro.objects.get(
                correo=user.email)
            populate_member(user, info_miembro)
        else:
            user.rol = User.BE
        return user


def populate_member(
        user: User, info_miembro: UsuarioMiembro):
    user.rol = info_miembro.rol
    user.oficina = info_miembro.oficina
