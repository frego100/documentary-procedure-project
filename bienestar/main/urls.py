from django.urls import path
from django.contrib.auth import views as auth_views
from .views import pages,autentication
from ayuda_integral.views import expedientes
from main.views import profile

app_name='bienestar'

urlpatterns = [
    path('', pages.HomeView.as_view(), name='home'),
    path('redirecting/', pages.dispatch_view, name='dispatch'),
    # path('login/', auth_views.LoginView.as_view(), name='login'),
    path('login/', autentication.login_view, name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('profile/', profile.profile_detail_view, name='profile'),
    path('expediente/reporte/', expedientes.ExpedienteReportView.as_view(), name='expediente-report'),
    path('expediente/reporte/<str:pk>/',expedientes.ExpedienteReporteVistaView.as_view(), name='vista-report'),
    path('expediente/reporte/actividad/<str:pk>/',expedientes.ExpedienteReporteActividadVistaView.as_view(), name='vista-report-actividad')
]