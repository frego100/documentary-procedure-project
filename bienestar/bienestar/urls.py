from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from django.conf.urls import handler404, handler500


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('main.urls')),
    path('ayuda-integral/', include('ayuda_integral.urls')),
    path('accounts/', include('allauth.urls')),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)

handler404 = 'main.views.error.error_404_view'
handler500 = 'main.views.error.error_500_view'
handler403 = 'main.views.error.error_403_view'
