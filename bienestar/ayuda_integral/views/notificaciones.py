from django.shortcuts import redirect
from django.views.generic import DetailView
from main.models import User, Expediente



class NotificacionView(DetailView):
    def get(self, request, pk):
        notification = request.user.notifications.get(pk=pk)
        notification.unread = False
        notification.save()
        action_object = notification.action_object
        if isinstance(action_object, Expediente):
            return redirect("ayuda_integral:expediente-detail", pk=action_object.pk)
        
        return redirect("ayuda_integral:expediente-list")