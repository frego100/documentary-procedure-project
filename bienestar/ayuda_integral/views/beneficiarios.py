from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse, reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.views.generic import *
from main import models
from main.models import Beneficiario, Entidad
from django import forms
from main.mixins import RolPermisosMixin,rol_permission_required
from django.db.models import Q
from django.contrib import messages

from ayuda_integral.forms import BeneficiarioForm, BeneficiarioUpdateForm

class SolicitantesExternosListView(LoginRequiredMixin,RolPermisosMixin, ListView):
    rol_permission_required = ('AS','SG','SO','SB','JO','TS')
    model = Beneficiario
    paginate_by = 10
    template_name = 'ayuda_integral/beneficiario/externo_list.html'

    def get_queryset(self):
        buscar = self.request.GET.get('buscar', '')
        return self.model.objects.filter(is_interno=False).filter(Q(nombres__icontains = buscar) | Q(apellidos__icontains = buscar) | Q(identificador__icontains = buscar))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['buscar'] = self.request.GET.get('buscar', '')
        return context


class SolicitantesInternosListView(LoginRequiredMixin,RolPermisosMixin, ListView):
    rol_permission_required = ('AS','SG','SO','SB','JO','TS')
    model = Beneficiario
    paginate_by = 10
    template_name = 'ayuda_integral/beneficiario/interno_list.html'

    def get_queryset(self):
        buscar = self.request.GET.get('buscar', '')
        return self.model.objects.filter(is_interno=True).filter(Q(nombres__icontains = buscar) | Q(apellidos__icontains = buscar) | Q(identificador__icontains = buscar))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['buscar'] = self.request.GET.get('buscar', '')
        return context


class BeneficiarioExternoDetailView(LoginRequiredMixin,RolPermisosMixin, DetailView):
    rol_permission_required = ('AS','SG','SO','SB','JO','TS')
    model = models.Beneficiario
    template_name = 'ayuda_integral/beneficiario/detail.html'

class BeneficiarioExternoCreateView(LoginRequiredMixin,RolPermisosMixin, CreateView):
    rol_permission_required = ('AS','SG')
    model = models.Beneficiario
    form_class = BeneficiarioForm
    
    template_name = 'ayuda_integral/beneficiario/create_form.html'
    
    def form_valid(self, form):
        benefExt = models.Beneficiario(
            is_interno = False,
            tipo_entidad = 'DE',
            tipo_rol = 'EX',
            nombres = form.cleaned_data['nombres'],
            apellidos = form.cleaned_data['apellidos'],
            identificador = form.cleaned_data['identificador'],
            entidad = form.cleaned_data['entidad']
        )
        benefExt.save()


    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['entidades'] = models.Entidad.objects.filter(Q(area=models.Entidad.C)|Q(area=models.Entidad.N))
        
        return context


    
    def get_success_url(self):
        return reverse('ayuda_integral:beneficiario-externo-detail', args=(self.object.pk,))

class BeneficiarioInternoCreateView(LoginRequiredMixin,RolPermisosMixin, CreateView):
    rol_permission_required = ('BE')
    model = models.Beneficiario
    form_class = BeneficiarioForm
    template_name = 'ayuda_integral/beneficiario/createinterno_form.html'
    
    def form_valid(self, form):
        nombres = form.cleaned_data['nombres']
        apellidos = form.cleaned_data['apellidos']
        identificador = form.cleaned_data['identificador']
        entidad = form.cleaned_data['entidad']
        alumno_registrointerno(self.request, identificador,nombres,apellidos,entidad,self.request.user)
        
        return redirect('ayuda_integral:solicitud-list')
    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['entidades'] = models.Entidad.objects.filter(Q(area=models.Entidad.I)|Q(area=models.Entidad.S)|Q(area=models.Entidad.B))        
        return context
    
    def get_success_url(self):
        return reverse('ayuda_integral:solicitud-list')

def  alumno_registrointerno(request, ID, NOMBRES, APELLIDOS, ENTIDAD,user):    
    if(user.beneficiario):
        messages.error(
            request,
            'Beneficiario ya se encuentra registrado',
            extra_tags='mensaje_solicitanteinterno',
        )
    else:

        if(not (Beneficiario.objects.filter(identificador=ID).exists())):
            print(ENTIDAD)
            alumno = models.Beneficiario(
                is_interno = True,
                tipo_entidad = 'ES',
                tipo_rol = 'AL',
                nombres = NOMBRES,
                apellidos = APELLIDOS,
                identificador = ID,
                entidad = Entidad.objects.get(nombre = ENTIDAD)
            )
            alumno.save()
            user.beneficiario = alumno
            user.save()
            messages.success(
                request,
                'Beneficiario registrado exitosamente',
                extra_tags='mensaje_solicitanteinterno',
            )
        else:
            bene = Beneficiario.objects.get(identificador=ID)
            user.beneficiario = bene
            user.save()
            messages.success(
                request,
                'Beneficiario registrado exitosamente',
                extra_tags='mensaje_solicitanteinterno',
            )
    return 


class BeneficiarioExternoUpdateView(LoginRequiredMixin,RolPermisosMixin, UpdateView):
    rol_permission_required = ('AS','SG')
    model = models.Beneficiario
    form_class = BeneficiarioForm
    template_name = 'ayuda_integral/beneficiario/update_form.html'
    
    def get_success_url(self):
        return reverse('ayuda_integral:beneficiario-externo-detail', args=(self.object.pk,))

class BeneficiarioExternoDeleteview(LoginRequiredMixin,RolPermisosMixin, DeleteView):
    rol_permission_required = ('AS','SG')
    model = models.Beneficiario
    template_name = 'ayuda_integral/beneficiario/delete_form.html'
    success_url = reverse_lazy('ayuda_integral:expediente-list')
