from main.models import UsuarioDerivacion, User, Expediente
from django.shortcuts import redirect
from django.urls import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from main.mixins import RolPermisosMixin,rol_permission_required
from notifications.signals import notify

from django.db.models import Max

class Derivacion():
    def __init__(self,
     expediente = None, emisor = None, receptor = None):
        self.expediente = expediente
        self.emisor = emisor
        self.receptor = receptor

def derivar(derivacion: Derivacion):
    # Crear un nuevo objeto usuario derivacion con los datos de derivacion
    UsuarioDerivacion.objects.create(
        usuario= derivacion.emisor,
        usuario_derivado= derivacion.receptor,
        expediente= derivacion.expediente,
    )

    #crear una notificacion para el receptor de la derivacion
    notify.send(
        derivacion.emisor, 
        recipient=derivacion.receptor, 
        action_object=derivacion.expediente,
        verb='Te han derivado un expediente',
        description=f'{derivacion.emisor.email} te ha derivado el expediente:'
    )

def derivacion_oficina(emisor, oficina, expediente):
    '''
    Send the newly created expediente to the office's secretary
    '''
    usuario_destino = User.objects.get(
        oficina = oficina,
        rol = User.SO
    )

    
    derivar(Derivacion(
        emisor=emisor,
        receptor=usuario_destino,
        expediente=expediente
    ))

@login_required()
@rol_permission_required(('AS','SG','SO'))
def derivacion_trabajador_view(request):
    '''
    Send a list of expedientes to a list of workers
    '''
    user = request.user
    if request.method == "POST":
        # Se obtiene la lista de pks de expedientes y de usuarios
        expedientes = request.POST.get('expedientes', [])
        receptores = request.POST.get('usuarios', [])

        if isinstance(expedientes, str):
            expedientes = [expedientes]
            
        if isinstance(receptores, str):
            receptores = [receptores]
        

        for expediente_pk in expedientes:
            for receptor_pk in receptores:
                expediente = Expediente.objects.get(pk= expediente_pk)
                receptor = User.objects.get(pk= receptor_pk)
                
                derivar(Derivacion(
                    emisor=user,
                    expediente=expediente,
                    receptor=receptor
                ))
                
                messages.success(
                    request,
                    f'Se derivó el expediente {expediente} al usuario {receptor} satisfactoriamente',
                )
                #asignar un numero de informe a cada expediente que se esta derivando
                max_inf = Expediente.objects.all().aggregate(Max('n_informe'))['n_informe__max']
                expediente.n_informe = max_inf + 1 if max_inf is not None else 1
                expediente.salida_informe = f'{expediente.n_informe:04d}-2021/OAIU-SDBU'
                
                expediente.save()
        
    return redirect(
        #reverse('ayuda_integral:expediente-list')
        #redireccionar a la misma pagina de donde derivamos
        request.META.get('HTTP_REFERER')
    )
