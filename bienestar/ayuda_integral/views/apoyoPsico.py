from django.urls import reverse, reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.views.generic import *
from main import api
from django import forms
from django.shortcuts import render
from main.mixins import RolPermisosMixin,rol_permission_required

@login_required()
@rol_permission_required(('AS','SG','SO','SB','JO','TS'))
def apoyoListView(request):
    solicitudes = api.apoyoPsicopedagogico()
    return render(request,template_name='ayuda_integral/apoyoPsico/list.html',context={'solicitudes':solicitudes})
    
@login_required()
@rol_permission_required(('AS','SG','SO','SB','JO','TS'))
def buscarSolicitudApoyoView(request):
    campo = request.POST.get('campo','')
    tipo = request.POST.get('tipo', '')
    solicitudes = {}
    if len(campo) != 0 and tipo == "DNI":
        solicitudes = api.obtenerSolicitudApoyoDNI(campo)
    elif len(campo) != 0 and tipo == 'CUI':
        solicitudes = api.obtenerSolicitudApoyoCUI(campo)
    else:
        print("no ingreso nada") 
    return render(request,template_name='ayuda_integral/apoyoPsico/list.html',context={'solicitudes':solicitudes})
