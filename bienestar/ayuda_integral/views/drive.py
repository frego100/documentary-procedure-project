from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
import os
from django.conf import settings

def obtenerFolderPorEmail(email):
    SCOPES = ['https://www.googleapis.com/auth/drive.readonly']
    KEY_FILE_LOCATION = os.path.join(settings.BASE_DIR,'bienestar/TramiteDocumentarioBienestar-c483db846e97.json')
    creds = ServiceAccountCredentials.from_json_keyfile_name(KEY_FILE_LOCATION, SCOPES)
    DRIVE = build('drive', 'v3', credentials=creds)

    response = DRIVE.files().list(q="mimeType = 'application/vnd.google-apps.folder' and '1BsY8Uytb_CqIgeRl-VraIGd8aYGeEq-m' in parents and name contains '" + email + "'",
                                            spaces='drive',
                                            fields='files(id, name, webViewLink, createdTime)',
                                            ).execute()

    if response.get('files', []):
        return response.get('files', [])[0]
    else:
        return None


def obtenerArchivos(folder):
    SCOPES = ['https://www.googleapis.com/auth/drive.readonly']
    KEY_FILE_LOCATION = os.path.join(settings.BASE_DIR,'bienestar/TramiteDocumentarioBienestar-c483db846e97.json')
    creds = ServiceAccountCredentials.from_json_keyfile_name(KEY_FILE_LOCATION, SCOPES)
    DRIVE = build('drive', 'v3', credentials=creds)
    files = []

    page_token = None
    while True:
        response = DRIVE.files().list(q="mimeType != 'application/vnd.google-apps.folder' and '" + folder + "' in parents",
                                            spaces='drive',
                                            fields='nextPageToken, files(id, name, webViewLink, createdTime)',
                                            pageToken=page_token).execute()
        for file in response.get('files', []):
            files.append(file)

        page_token = response.get('nextPageToken', None)
        if page_token is None:
            break
    
    return files

def eliminarNumerosInicialesDeNombre(nombre):
    if(len(nombre) > 18 and nombre[0:10].isdigit() ):
        nombre = nombre[11:]
        contador = 1
        while nombre[0].isdigit():
            nombre = nombre[1:]
            if contador == 7:
                break
            contador += 1
    return nombre