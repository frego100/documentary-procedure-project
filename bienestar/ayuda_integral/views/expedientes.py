from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.edit import DeleteView 
from django.urls import reverse, reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.views.generic import *
from main import models
from main.models import Entidad,Expediente, User, Beneficiario, SubActividad, HistorialDeExpediente, Actividad
from django import forms
from django.contrib.auth.mixins import PermissionRequiredMixin
from main.mixins import RolPermisosMixin,rol_permission_required
from main import api
from .derivacion import Derivacion, derivar
from django.shortcuts import render
from openpyxl import Workbook
from openpyxl.utils import get_column_letter
from openpyxl.styles import PatternFill, Border, Side, Alignment, Protection, Font, GradientFill, NamedStyle
from django.http.response import HttpResponse
import json
import datetime
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Max

from .derivacion import derivacion_oficina

from ayuda_integral.forms import ExpedienteForm, ExpedienteUpdateForm, DocumentoExpedienteForm
from django.utils.timezone import datetime

from notifications.signals import notify

class ExpedienteListView(LoginRequiredMixin,RolPermisosMixin, ListView):
    rol_permission_required = ('AS','SB','JO','TS','SO','SG')
    model = models.Expediente
    paginate_by = 10
    expedientes = models.Expediente
    template_name = 'ayuda_integral/expediente/list.html'
    def get_queryset(self):
        buscar = self.request.GET.get('buscar', '')
        tipo = self.request.GET.get('tipo', '')
        mostrarConcluidos = self.request.GET.get('mostrar_concluidos', 'no')
        desde = self.request.GET.get('desde','')
        hasta = self.request.GET.get('hasta','')
        #expedientes mostrados a Jefe de oficina (JO)
        expedientes = Expediente.objects.filter(derivados__usuario_derivado = self.request.user)
        #AS y SG podran ver todos los expedientes
        if(self.request.user.rol == 'AS' or self.request.user.rol == 'SG' or self.request.user.rol == 'SB'):
            expedientes = Expediente.objects.all()
        #SO y JO podran ver los expedientes derivados a su oficina
        elif(self.request.user.rol == 'SO' or self.request.user.rol == 'JO' ):
            expedientes = Expediente.objects.filter(derivados__usuario_derivado__oficina = self.request.user.oficina, derivados__usuario_derivado__rol = 'SO')

        if(mostrarConcluidos != 'no'):
            expedientes = expedientes.filter(estado_expediente = Expediente.CO)
        if desde =="" and hasta=="":
            desde = '2021-01-01'
            hasta = datetime.today()
        if desde == "":
                desde = '2021-01-01'
        elif hasta == "":
                hasta = datetime.today()
        if buscar == "":
            expedientes = expedientes.filter(fecha_recepcion__range=(desde, hasta))
        if tipo == "beneficiario":
            expedientes = expedientes.filter(fecha_recepcion__range=(desde, hasta)).filter(beneficiario__nombres__icontains = buscar) | expedientes.filter(beneficiario__apellidos__icontains = buscar)
        elif tipo == "entidad":
            expedientes = expedientes.filter(fecha_recepcion__range=(desde, hasta)).filter(beneficiario__entidad__nombre__icontains = buscar)
        elif tipo == "area":
            expedientes = expedientes.filter(fecha_recepcion__range=(desde, hasta)).filter(beneficiario__entidad__area__icontains = buscar)
        elif tipo == "actividad":
            expedientes = expedientes.filter(fecha_recepcion__range=(desde, hasta)).filter(actividad__nombre__icontains = buscar)
        elif tipo == "subactividad":
            expedientes = expedientes.filter(fecha_recepcion__range=(desde, hasta)).filter(sub_actividad__nombre__icontains = buscar)
        elif tipo == "expediente":
            expedientes = expedientes.filter(fecha_recepcion__range=(desde, hasta)).filter(numero_expediente__icontains = buscar)
        elif tipo == "proveido":
            expedientes = expedientes.filter(fecha_recepcion__range=(desde, hasta)).filter(numero_proveido__icontains = buscar)
        elif tipo == "oficio":
            expedientes = expedientes.filter(fecha_recepcion__range=(desde, hasta)).filter(numero_oficio__icontains = buscar)
        expedientes = expedientes.order_by('numero_expediente')
        return expedientes.order_by('-fecha_recepcion')

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['contador'] = self.get_queryset().count()
        context['expedientes'] = models.Expediente.objects.exclude(estado_expediente=models.Expediente.CO).exclude(derivados__usuario=self.request.user).filter(derivados__usuario_derivado = self.request.user)
        context['usuarios'] = models.User.objects.exclude(
            pk= self.request.user.pk
        ).filter(
            rol = 'TS'
        )
        context['mostrarConcluidos'] = self.request.GET.get('mostrar_concluidos', 'no')
        context['buscar'] = self.request.GET.get('buscar', '')
        context['desde'] = self.request.GET.get('desde', '')
        context['hasta'] = self.request.GET.get('hasta', '')
        context['tipo'] = self.request.GET.get('tipo', '')

        return context

def compararExpediente(pk, expediente):
    expedientes = models.UsuarioDerivacion.objects.filter(usuario_derivado = pk, expediente = expediente)
    if expedientes:
        return True
    else:
        return False

class ExpedienteDetailView(LoginRequiredMixin,RolPermisosMixin, DetailView):
    rol_permission_required = ('AS','JO','TS','SG','SB', 'SO')
    model = models.Expediente
    template_name = 'ayuda_integral/expediente/detail.html'

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.rol =="TS":
            if not compararExpediente(self.request.user,self.get_object()):
                return self.handle_no_permission()

        return super().dispatch(request, *args, **kwargs)
        
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        expediente = self.get_object()
        if not expediente.usuario_beneficiario:
            context['docPersonal'] = None
        else:
            context['docPersonal'] = expediente.usuario_beneficiario.documento_set.all()
        context['usuarios'] = models.User.objects.exclude(
            pk= self.request.user.pk
        ).filter(
            rol = 'TS'
        )
        context['se_puede_derivar'] = models.UsuarioDerivacion.objects.filter(usuario=self.request.user, expediente=self.get_object()).count() == 0
        return context

class ExpedienteDocsCreateView(LoginRequiredMixin,RolPermisosMixin, CreateView):
    rol_permission_required = ('AS','JO','TS','SG','SB','SO')
    model = models.DocumentoExpediente
    form_class = DocumentoExpedienteForm

    template_name = 'ayuda_integral/expediente/document_create.html'
    success_url = reverse_lazy('ayuda_integral:expediente-list')
    def form_valid(self, form):
        documento = models.DocumentoExpediente(
            nombre = form.cleaned_data['nombre'],
            path = form.cleaned_data['path'],
            expediente =models.Expediente.objects.get(pk = self.kwargs.get('pk'))
        )
        documento.save()
        return redirect('ayuda_integral:expediente-detail', self.kwargs.get('pk'))

class ExpedienteDocsDeleteView(DeleteView): 
    model = models.DocumentoExpediente 

    def get_success_url(self):
        return self.request.META.get('HTTP_REFERER')

class ExpedienteCreateView(LoginRequiredMixin,RolPermisosMixin, CreateView):
    rol_permission_required = ('AS','SG')
    form_class = ExpedienteForm
    
    template_name = 'ayuda_integral/expediente/create_form.html'
    success_url = reverse_lazy('ayuda_integral:expediente-list')

    def form_valid(self, form):
        beneficiario = None
        solicitud = None
        usuarioBeneficiario = None
        creado_a_partir_de_solicitud = False
        if( self.kwargs.get('pk')):
            # Caso que entra por parte de una solicitud
            # obtencion del id de la solicitud que creo el expediente
            solicitud = models.Solicitud.objects.get(pk=self.kwargs.get("pk"))
            beneficiario = solicitud.usuario.beneficiario
            creado_a_partir_de_solicitud = True
        else:
            beneficiario = form.cleaned_data['beneficiario']


        try:
            usuarioBeneficiario = beneficiario.user
        except ObjectDoesNotExist:
            usuarioBeneficiario = None

        expediente = models.Expediente(
            fecha_recepcion=form.cleaned_data['fecha_recepcion'],
            numero_expediente=form.cleaned_data['numero_expediente'],
            descripcion=form.cleaned_data['descripcion'],
            numero_proveido=form.cleaned_data['numero_proveido'],
            numero_oficio=form.cleaned_data['numero_oficio'],
            beneficiario=beneficiario,
            usuario_beneficiario=usuarioBeneficiario, #el usuario vinculado con beneficiario
            actividad=form.cleaned_data['actividad'],
            sub_actividad=form.cleaned_data['sub_actividad'],
        )
        if(creado_a_partir_de_solicitud):
            #asignar n_expediente_solicitud
            max_exp_sol = models.Expediente.objects.all().aggregate(Max('n_expediente_solicitud'))['n_expediente_solicitud__max']
            expediente.n_expediente_solicitud = max_exp_sol + 1 if max_exp_sol is not None else 1
            #asignar numero de expediente
            expediente.numero_expediente = f'{expediente.n_expediente_solicitud:04d}-2021/SDBUint'
            #copiar salida_expediente
            expediente.salida_expediente = expediente.numero_expediente
        else:
            max_exp = models.Expediente.objects.all().aggregate(Max('n_expediente'))['n_expediente__max']
            expediente.n_expediente = max_exp + 1 if max_exp is not None else 1
            expediente.salida_expediente = f'{expediente.n_expediente:04d}-2021/SDBU'

        salida_a_crear = self.request.POST.get('salida_a_crear', None)

        if(salida_a_crear == 'oficio'):
            max_ofi = models.Expediente.objects.all().aggregate(Max('n_oficio'))['n_oficio__max']
            expediente.n_oficio = max_ofi + 1 if max_ofi is not None else 1
            expediente.salida_oficio = f'{expediente.n_oficio:04d}-2021-SDBU-DIGA/UNSA'    
        elif(salida_a_crear == 'proveido'):
            max_pro = models.Expediente.objects.all().aggregate(Max('n_proveido'))['n_proveido__max']
            expediente.n_proveido = max_pro + 1 if max_pro is not None else 1
            expediente.salida_proveido = f'{expediente.n_proveido:04d}-2021/SDBU'

        expediente.save()

        #actualizacion de solicitud con el expediente creado
        if(solicitud != None):
            models.Solicitud.objects.filter(pk = solicitud.id).update(expediente=expediente)

        derivacion_oficina(
            self.request.user, 
            self.request.POST['oficina_a_derivar'], 
            expediente
        )
        
        return redirect('ayuda_integral:expediente-detail', expediente.id)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['oficinas'] = User.OFICINAS_POSIBLES
        context['beneficiario'] = models.Beneficiario.objects.all()
        context['actividad'] = models.Actividad.objects.all()
        context['subactividad'] = models.SubActividad.objects.all()
        if self.kwargs.get('pk'):
            context['pk'] = self.kwargs.get('pk')
        return context


class ExpedienteUpdateView(LoginRequiredMixin,RolPermisosMixin, UpdateView):
    rol_permission_required = ('AS','SB','JO','TS')
    form_class = ExpedienteUpdateForm
    model = Expediente

    template_name = 'ayuda_integral/expediente/update_form.html'

    def get_success_url(self):
        return reverse('ayuda_integral:expediente-detail', args=(self.object.pk,))

    def dispatch(self, request, *args, **kwargs):

        if self.request.user.rol =="TS":
            if not compararExpediente(self.request.user,self.get_object()):
                return self.handle_no_permission()

        if self.get_object().estado_expediente == Expediente.CO:
            return redirect('ayuda_integral:expediente-detail', self.kwargs.get('pk'))
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        if 'estado_expediente' in form.changed_data: #estado_expediente ha cambiado:
            expediente = form.save(commit = False)
            #si el estado cambia a concluido, entonces se asigna la fecha de salida con la fecha actual.
            if expediente.estado_expediente == Expediente.CO:
                expediente.fecha_salida = datetime.datetime.now()
                #se notifica a la secretaria general, que el expediente ha concluido
                notify.send(
                    self.request.user, 
                    recipient=User.objects.get(
                        oficina = User.AI,
                        rol = User.SG
                    ), 
                    action_object=expediente,
                    verb=f'{expediente.numero_expediente} ha concluido.',
                    description=f'{self.request.user.email} ha cambiando el estado del expediente a CONCLUIDO.'
                )

            expediente.save()
            HistorialDeExpediente.objects.create(
                usuario = self.request.user,
                titulo = 'Estado cambio a ' + dict(form.fields['estado_expediente'].choices)[form.data['estado_expediente']],
                descripcion = ' ha cambiado el estado de este expediente.',
                expediente = expediente,
            )

        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


@login_required()
@rol_permission_required(('AS'))
def delete_view(request, pk):
    if request.method == 'POST':
        expediente = models.Expediente.objects.get(pk = pk)
        expediente.delete()
    return redirect('ayuda_integral:expediente-list')

   
def consulta_expedientes(pk, user):
    print( f'usuario: {user}')
    
    expedientes = models.Expediente.objects.filter(
        beneficiario__entidad__nombre = pk).order_by('numero_expediente')  | Expediente.objects.filter(actividad__nombre = pk).order_by('numero_expediente') | Expediente.objects.filter(sub_actividad__nombre = pk).order_by('numero_expediente')
    if pk == 'Ingeniería':
        expedientes = Expediente.objects.filter(beneficiario__entidad__area = 'I').order_by('numero_expediente')
    if pk == 'Sociales':
        expedientes = Expediente.objects.filter(beneficiario__entidad__area = 'S').order_by('numero_expediente')
    if pk == 'Biomedicas':
        expedientes = Expediente.objects.filter(beneficiario__entidad__area = 'B').order_by('numero_expediente')
    if pk == 'Oficina Independiente':
        expedientes = Expediente.objects.filter(beneficiario__entidad__area = 'N').order_by('numero_expediente')
    if pk == 'Alumnos':
        expedientes = Expediente.objects.filter(beneficiario__tipo_rol="AL").order_by('numero_expediente')
    if pk == 'Profesores':
        expedientes = Expediente.objects.filter(beneficiario__tipo_rol="PR").order_by('numero_expediente')
    if pk == 'Administrativos':
        expedientes = Expediente.objects.filter(beneficiario__tipo_rol="AD").order_by('numero_expediente')

    if( user.rol == "TS"):
        
        for expediente in expedientes:
            expes = models.UsuarioDerivacion.objects.filter(usuario_derivado = user, expediente = expediente)
            if expes.count() == 0:
                expedientes =expedientes.exclude(numero_expediente = expediente.numero_expediente)
            
        return expedientes
    elif user.rol == 'JO' or user.rol == 'SO':

        for expediente in expedientes:
            expes = models.UsuarioDerivacion.objects.filter(usuario_derivado__oficina = 'AI', expediente = expediente)
            if expes.count() == 0:
                expedientes =expedientes.exclude(numero_expediente = expediente.numero_expediente)
        return expedientes
    else:
        return expedientes
    return expedientes

class ExpedienteReporteActividadVistaView(LoginRequiredMixin,RolPermisosMixin,ListView):
    rol_permission_required = ('AS','SO','SB','JO','TS')
    model = models.Expediente
    paginate_by = 10
    template_name = "ayuda_integral/expedientes_report_actividad.html"
    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        pk = self.kwargs.get('pk')
        expedientes = {}
        subactividades = SubActividad.objects.filter(actividad__nombre = pk)
        for subactividad in subactividades:
            nombre = subactividad.nombre
            expNomb = Expediente.objects.filter(sub_actividad=subactividad).count()
            expedientes[nombre] = expNomb
        context['tipo'] = expedientes
        return context

class ExpedienteReporteVistaView(LoginRequiredMixin,RolPermisosMixin,ListView):
    rol_permission_required = ('AS','SO','SB','JO','TS')
    model = models.Expediente
    paginate_by = 10
    template_name = "ayuda_integral/vista_report.html"
    def get_queryset(self):
        pk = self.kwargs.get('pk')
        expedientes = consulta_expedientes(pk,self.request.user)
        return expedientes.order_by('-fecha_recepcion')
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filtro'] = self.kwargs.get('pk')
        return context
    
def obtener_expedientes_derivados(user, expedientes):
    cont =0;
    if( user.rol == "TS"):
        
        for expediente in expedientes:
            cont = cont + models.UsuarioDerivacion.objects.filter(usuario_derivado = user, expediente = expediente).values('expediente').distinct().count()
        return cont

    elif user.rol == 'JO' or user.rol == 'SO':

        for expediente in expedientes:
            cont = cont +models.UsuarioDerivacion.objects.filter(usuario_derivado__oficina = 'AI', expediente = expediente).values('expediente').distinct().count()
        return cont

    else:

        return expedientes.count()

class ExpedienteReportView(LoginRequiredMixin,RolPermisosMixin,ListView):
    rol_permission_required = ('AS','SO','SB','JO','TS')
    model = models.Expediente
    paginate_by = 10
    template_name = "ayuda_integral/expedientes_report.html"
    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['areas'] = models.Entidad.AREAS_POSIBLES
        categoria = self.request.GET.get('tipo','')
        user = self.request.user
        print(user.rol)
        expedientes = {}
        desde = self.request.GET.get('desde','')
        hasta = self.request.GET.get('hasta','')
        if desde =="" and hasta=="":
            desde = '2021-01-01'
            hasta = datetime.today()
        if desde == "":
            desde = '2021-01-01'
        elif hasta == "":
            hasta = datetime.today()
        if categoria == "area":
            # obtener_expedientes_derivados(rol,"2s")
            # usuario_derivacion = UsuarioDerivacion.objects.filter
            # areas = Expediente.objects.filter(beneficiario__entidad__area = 'I').count() | Expediente.objects.filter(beneficiario__entidad__area = 'S').count() | Expediente.objects.filter(beneficiario__entidad__area = 'B').count() | Expediente.objects.filter(beneficiario__entidad__area = 'N').count(),
            expedientes = {"Ingeniería" : obtener_expedientes_derivados(
                user, Expediente.objects.filter(fecha_recepcion__range=(desde, hasta)).filter(beneficiario__entidad__area = 'I')),
            "Sociales" : obtener_expedientes_derivados(
                user, Expediente.objects.filter(fecha_recepcion__range=(desde, hasta)).filter(beneficiario__entidad__area = 'S')),
            "Biomedicas" : obtener_expedientes_derivados(
                user, Expediente.objects.filter(fecha_recepcion__range=(desde, hasta)).filter(beneficiario__entidad__area = 'B')),
            "Oficina Independiente" : obtener_expedientes_derivados(
                user, Expediente.objects.filter(fecha_recepcion__range=(desde, hasta)).filter(beneficiario__entidad__area = 'N')),
            "Central" : obtener_expedientes_derivados(
                user, Expediente.objects.filter(fecha_recepcion__range=(desde, hasta)).filter(beneficiario__entidad__area = 'C')
            )
            }
        if categoria == "beneficiarios":
            expedientes = {"Alumnos" : obtener_expedientes_derivados(
                user, Expediente.objects.filter(fecha_recepcion__range=(desde, hasta)).filter(beneficiario__tipo_rol="AL")),
            "Profesores" : obtener_expedientes_derivados(
                user, Expediente.objects.filter(fecha_recepcion__range=(desde, hasta)).filter(beneficiario__tipo_rol="PR")),
            "Administrativos" : obtener_expedientes_derivados(
                user, Expediente.objects.filter(fecha_recepcion__range=(desde, hasta)).filter(beneficiario__tipo_rol="AD")),
            }
        if categoria == "escuela":
            escuelas = Entidad.objects.filter(area="I") | Entidad.objects.filter(area="S") | Entidad.objects.filter(area="B")
            for escuela in escuelas:
                nombre = escuela.nombre
                expNomb = obtener_expedientes_derivados(
                user, Expediente.objects.filter(fecha_recepcion__range=(desde, hasta)).filter(beneficiario__entidad__nombre = nombre))
                expedientes[nombre] = expNomb
            # expedientes["ingenieria"] = Expediente.objects.filter(beneficiario__entidad__area = 'I').count()

        if categoria == "actividad":
            actividades = Actividad.objects.all()
            for actividad in actividades:
                nombre = actividad.nombre
                expNomb = obtener_expedientes_derivados(
                user, Expediente.objects.filter(fecha_recepcion__range=(desde, hasta)).filter(actividad=actividad))
                if(SubActividad.objects.filter(fecha_recepcion__range=(desde, hasta)).filter(actividad__nombre = nombre).count() ==0):
                    expNomb = -1 *expNomb
                expedientes[nombre] = expNomb
            context['tipo_'] = 'actividad'
        # print(expedientes)
        context['tipo'] = expedientes
        context['desde'] = desde
        context['hasta'] = hasta
        context['categoria'] = categoria
        return context

@login_required()
@rol_permission_required(('AS','SG','SO','SB','JO','TS'))
def alumnos_list_view(request):
    """

    Metodo para mostrar todos los alumnos del API de sistema de la UNSA
    Parámetros:
    request -- peticion que obtendra el numero de la pagina en nuestro template

    """
    # obtencion del valor page del template para hacer la paginacion
    page = request.GET.get('page')
    #obtener datos de paginacion
    return paginar_lista(request, page,'alumno')

@login_required()
@rol_permission_required(('AS','SG','SO','SB','JO','TS'))
def docentes_list_view(request):
    '''

    Metodo para mostrar todos los docentes del API de sistema de la UNSA
    Parámetros:
    request -- peticion que obtendra el numero de la pagina en nuestro template

    '''
    # obtencion del valor page del template para hacer la paginacion
    page = request.GET.get('page')
    #obtener datos de paginacion
    return paginar_lista(request, page,'docente')

@login_required()
@rol_permission_required(('AS','SG','SO','SB','JO','TS'))
def administrativos_list_view(request):
    '''

    Metodo para mostrar todos los administrativos del API de sistema de la UNSA
    Parámetros:
    request -- peticion que obtendra el numero de la pagina en nuestro template

    '''
    # obtencion del valor page del template para hacer la paginacion
    page = request.GET.get('page')
    #obtener datos de paginacion
    return paginar_lista(request, page,'administrativo')


def paginar_lista(request, page, tipo):
    '''
    Metodo para paginar los datos devueltos por el api externo
    Parametros:
    page -- pagina actual
    tipo -- (alumno, docente o administrativo)
    '''
    tipos=f'{tipo}s'
    if page and int(page) > 0:
        page = int(page)
        beneficiarios = api.beneficiarios(page,tipos)
        return render(request,template_name=f'ayuda_integral/beneficiario/{tipo}_list.html',context={
            'beneficiarios':beneficiarios,
            'previous':page -1,
            'next':page + 1})
    # caso que es la primera vez ingresando al template
    else:
        beneficiarios = api.beneficiarios(1,tipos)
        return render(request,template_name=f'ayuda_integral/beneficiario/{tipo}_list.html',context={
            'beneficiarios':beneficiarios,
            'next':2})

@login_required()
@rol_permission_required(('AS','SG'))
def buscar_beneficiario_view(request):
    """

    Metodo para buscar usuarios dentro del API del sistema externo de la UNSA
    Parámetros:
    request -- datos mandados del template de alumnos-list(nombres, apellidos) a traves de un form

    """
    # campos obtenidos de nuestro formulario
    nombres = request.POST.get('nombres','')
    apellidos = request.POST.get('apellidos','')
    tipo = request.POST.get('tipo','')

    benes ={}
    # si ingreso campo apellidos y nombres
    if len(nombres) !=0 and len(apellidos) !=0:
        benes = api.obtenerBeneficiarios(apellidos,nombres, tipo)
    # si ingreso solo apellidos
    elif len(nombres) ==0 and len(apellidos) !=0:
        benes = api.obtenerBeneficiarioApellido(apellidos, tipo)
    # si ingreso solo nombres
    elif len(nombres) !=0 and len(apellidos) ==0:
        benes = api.obtenerBeneficiarioNombre(nombres, tipo)
    # Devolvemos al template un diccionario con el o los beneficiarios
    return render(request,template_name='ayuda_integral/beneficiario/beneficiario_api.html',context={'beneficiarios':benes, 'tipo':tipo})

@login_required()
@rol_permission_required(('AS','SG'))
def  beneficiario_registro(request):
    if request.method == 'POST':
        TIPO = request.POST.get('TIPO','')
        CUI = request.POST.get('CUI','')
        DNI = request.POST.get('DNI','')
        NOMBRE = request.POST.get('NOMBRE','')
        ESCUELA = request.POST.get('ESCUELA','')
        DEPENDENCIA = request.POST.get('DEPENDENCIA','')
        print("DNI: "+DNI)
        if TIPO == 'alumnos':
            alumno_registro(CUI, NOMBRE, ESCUELA)
        if TIPO == 'docentes':
            docente_registro(DNI, NOMBRE, ESCUELA)
        if TIPO == 'administrativos':
            administrativo_registro(DNI, NOMBRE, DEPENDENCIA)
    return redirect("ayuda_integral:beneficiario-busqueda")

def  alumno_registro(CUI, NOMBRE, ESCUELA):
    print("alumno "+CUI)
    if(not (Beneficiario.objects.filter(identificador=CUI).exists())):
        alumno = models.Beneficiario(
            is_interno = True,
            tipo_entidad = 'ES',
            tipo_rol = 'AL',
            nombres = NOMBRE[NOMBRE.find(",")+2:len(NOMBRE)],
            apellidos = NOMBRE[0:NOMBRE.find(",")].replace("/"," "),
            identificador = CUI,
            entidad = Entidad.objects.filter(nombre__iregex = ESCUELA)[0]
        )
        alumno.save()
        print("alumno registrado")
    return redirect("ayuda_integral:beneficiario-busqueda")

def  docente_registro(DNI, NOMBRE, ESCUELA):
    if(not (Beneficiario.objects.filter(identificador=DNI).exists())):
        docente = models.Beneficiario(
            is_interno = True,
            tipo_entidad = 'ES',
            tipo_rol = 'PR',
            nombres = NOMBRE[NOMBRE.find(",")+2:len(NOMBRE)],
            apellidos = NOMBRE[0:NOMBRE.find(",")].replace("/"," "),
            identificador = DNI,
            entidad = Entidad.objects.filter(nombre__iregex = ESCUELA)[0]
        )
        docente.save()
    return redirect("ayuda_integral:beneficiario-busqueda")

def  administrativo_registro(DNI, NOMBRE, DEPENDENCIA):
    if(not (Beneficiario.objects.filter(identificador=DNI).exists())):
        administrativo = models.Beneficiario(
            is_interno = True,
            tipo_entidad = 'DP',
            tipo_rol = 'AD',
            nombres = NOMBRE[NOMBRE.find(",")+2:len(NOMBRE)],
            apellidos = NOMBRE[0:NOMBRE.find(",")].replace("/"," "),
            identificador = DNI,
            entidad = Entidad.objects.filter(nombre__iregex = DEPENDENCIA)[0]
        )
        administrativo.save()
    return redirect("ayuda_integral:beneficiario-busqueda")

def exportar_reporte(request):
    filtro = request.POST.get('FILTRO','')
    expedientes = consulta_expedientes(filtro, request.user).order_by('-fecha_recepcion')
    wb = Workbook()
    ws = wb.active

    ws.column_dimensions[get_column_letter(2)].width = 14
    ws.column_dimensions[get_column_letter(3)].width = 17
    ws.column_dimensions[get_column_letter(4)].width = 30
    ws.column_dimensions[get_column_letter(5)].width = 40
    ws.column_dimensions[get_column_letter(6)].width = 25
    ws.column_dimensions[get_column_letter(7)].width = 25
    ws.column_dimensions[get_column_letter(8)].width = 25

    cabecera = NamedStyle(name="cabecera")
    cabecera.font = Font(color="FF0000")
    thin = Side(border_style="thin", color="000000")
    cabecera.border = Border(top=thin, left=thin, right=thin)
    cabecera.alignment = Alignment(horizontal="center", vertical="center")
    cabecera.fill = PatternFill("solid", fgColor="DDDDDD")
    wb.add_named_style(cabecera)

    detalle = NamedStyle(name="detalle")
    detalle.border = Border(top=thin, left=thin, right=thin, bottom=thin)
    wb.add_named_style(detalle)

    ws['B2'] = 'REPORTE DE EXPEDIENTES'
    ws.merge_cells('B2:G2')

    ws['B3'] = 'N° EXPEDIENTE'
    ws['C3'] = 'FECHA DE INGRESO'
    ws['D3'] = 'APELLIDOS Y NOMBRES'
    ws['E3'] = 'DEPENDENCIA O ESCUELA'
    ws['F3'] = 'ASUNTO'
    ws['G3'] = 'SUBACTIVIDAD'
    ws['H3'] = 'RESULTADO'

    fila = 2
    columna = 2
    for i in range(2):
        for j in range(7):
            ws.cell(row = fila, column = columna).style = 'cabecera'
            columna+=1
        columna = 2
        fila+=1
        
    fila = 4
    for i in expedientes:
        for j in range(7):
            ws.cell(row = fila, column = columna).style = 'detalle'
            columna+=1
        columna = 2
        fila+=1

    cont = 4
    for exp in expedientes:
        ws.cell(row = cont, column = 2).value = exp.numero_expediente
        ws.cell(row = cont, column = 3).value = exp.fecha_recepcion
        ws.cell(row = cont, column = 3).number_format="DD/MM/YYYY"
        ws.cell(row = cont, column = 4).value = f"{exp.beneficiario.apellidos} {exp.beneficiario.nombres}"
        ws.cell(row = cont, column = 5).value = exp.beneficiario.entidad.nombre
        ws.cell(row = cont, column = 6).value = exp.actividad.nombre
        ws.cell(row = cont, column = 7).value = exp.sub_actividad.nombre
        ws.cell(row = cont, column = 8).value = exp.resultado
        cont+=1
    
    nombre_archivo = "ReporteExpedientes.xlsx"
    response = HttpResponse(content_type = "application/ms-excel")
    content = "attachment; filename = {0}".format(nombre_archivo)
    response['Content-Disposition'] = content
    wb.save(response)
    return response
