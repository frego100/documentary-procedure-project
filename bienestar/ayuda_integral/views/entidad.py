from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse, reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.views.generic import *
from main import models
from main.models import Entidad
from django import forms
from main.mixins import RolPermisosMixin,rol_permission_required

from ayuda_integral.forms import EntidadForm

class EntidadListView(LoginRequiredMixin,RolPermisosMixin, ListView):
    rol_permission_required = ("AS","SG","SO","SB","JO","TS")
    model = models.Entidad
    paginate_by=10
    entidad = models.Entidad
    template_name = 'ayuda_integral/entidad/list.html'
    def get_queryset(self):
            return self.model.objects.all()

class EntidadDetailView(LoginRequiredMixin, RolPermisosMixin, DetailView):
    rol_permission_required = ("AS","SG","SB")
    model = models.Entidad
    template_name = 'ayuda_integral/entidad/detail.html'
class EntidadCreateView(LoginRequiredMixin, RolPermisosMixin, CreateView):
    rol_permission_required = ("AS","SG","SB")
    model = models.Entidad
    form_class = EntidadForm
    template_name = 'ayuda_integral/entidad/create_form.html'
    success_url = reverse_lazy('ayuda_integral:entidad-list')
    
class EntidadUpdateView(LoginRequiredMixin,RolPermisosMixin, UpdateView):
    rol_permission_required = ("AS","SG","SB")
    model = models.Entidad
    form_class = EntidadForm
    template_name = 'ayuda_integral/entidad/update_form.html'
    success_url = reverse_lazy('ayuda_integral:entidad-list')

class EntidadDeleteView(LoginRequiredMixin,RolPermisosMixin, DeleteView):
    rol_permission_required = ("AS","SG","SB")
    model = models.Entidad
    template_name = 'ayuda_integral/entidad/delete_form.html'
    success_url = reverse_lazy('ayuda_integral:entidad-list')
