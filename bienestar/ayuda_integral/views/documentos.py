from django.views.generic import ListView, DetailView, CreateView
from django.urls import reverse, reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from main import models
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.shortcuts import render
from main.mixins import RolPermisosMixin,rol_permission_required

from ayuda_integral.forms import DocumentoExpedienteBeneficiarioForm

@login_required()
@rol_permission_required(('BE'))
def documento_list_view(request,pk):
    if request.user.rol == 'BE':
        documentos = models.Documento.objects.filter(usuario = request.user.id)
    else: 
        documentos = models.Documento.objects.filter(usuario = pk)
    beneficiario = models.User.objects.get(pk =request.user.id)
    return render(request,template_name='documento/documento_list.html',context={'docs': documentos,'beneficiario': beneficiario, 'pk':pk})

class ExpedienteDocsBeneficiarioCreateView(LoginRequiredMixin,RolPermisosMixin, CreateView):
    rol_permission_required = ('BE')
    model = models.Documento
    form_class = DocumentoExpedienteBeneficiarioForm

    template_name = 'documento/beneficiario_prueba_subida.html'
    success_url = reverse_lazy('ayuda_integral:documento-list')
    def form_valid(self, form):
        doc = models.Documento(
            usuario = models.User.objects.get(pk = self.request.user.id),
            nombre = form.cleaned_data['nombre'],
            path = form.cleaned_data['path'],
        )
        doc.save()
        
        return redirect('ayuda_integral:documento-list', self.kwargs.get('pk'))

    # def dispatch(self, request, *args, **kwargs):
    #     return redirect('ayuda_integral:solicitud-list')
