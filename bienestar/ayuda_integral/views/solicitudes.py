from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.urls import reverse, reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.shortcuts import render
from main import models
from main import api
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from main.models import Beneficiario, Solicitud, Documento, User, Entidad
from main.mixins import RolPermisosMixin,rol_permission_required
from django.contrib import messages
from django.db.models import Exists
from openpyxl import Workbook
from openpyxl.utils import get_column_letter
from openpyxl.styles import PatternFill, Border, Side, Alignment, Protection, Font, GradientFill, NamedStyle
from django.http.response import HttpResponse

from ayuda_integral.forms import SolicitudForm
from ayuda_integral.views import drive

import json
import os
from django.conf import settings

from django.db.models import Q

from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

class SolicitudListView(LoginRequiredMixin,RolPermisosMixin, ListView):
    rol_permission_required = ('AS','SG','SB','BE')
    model = Solicitud
    paginate_by = 10
    template_name = 'ayuda_integral/solicitud/list.html'

    def get_queryset(self):
        
        
        if(self.request.user.rol == 'BE'):
            return self.model.objects.filter(usuario__pk =self.request.user.id).order_by('-id')
        else:
            buscar = self.request.GET.get('buscar', '')
            return self.model.objects.all().order_by('-id').filter(
                Q(usuario__nombres__icontains=buscar) |
                Q(usuario__apellidos__icontains=buscar) | 
                Q(usuario__email__icontains=buscar) |
                Q(expediente__numero_expediente__icontains=buscar) |
                Q(usuario__beneficiario__identificador__icontains=buscar)
            )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        usuario = self.request.user
        APELLIDOS = usuario.apellidos
        NOMBRES = usuario.nombres
        context['buscar'] = self.request.GET.get('buscar', '')
        if(self.request.user.rol == 'BE'):
            solicitante = api.obtenerBeneficiarios(APELLIDOS, NOMBRES,'alumnos')
            comensal = api.obtenerBeneficiarios(APELLIDOS, NOMBRES,'comedor')
            print(comensal)
            # if len(comensal) != 0:
            #     context['comensalExiste'] = 1
            # else:
            #     context['comensalExiste']= 0
            # if len(solicitante) != 0:
            #     context['existe'] = 1
            # else:
            if usuario.beneficiario:
                cui = usuario.beneficiario.identificador
                if Exists(Beneficiario.objects.filter(identificador = cui)):
                    context['existe'] = 1        
                else:
                    context['existe']=0
            else:
                context['existe']=0
        return context
                

class SolicitudDetailView(LoginRequiredMixin,RolPermisosMixin, DetailView):
    rol_permission_required = ('AS','SG','SB','BE')
    model = Solicitud
    template_name = 'ayuda_integral/solicitud/detail.html'

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.rol =="BE":
            if self.request.user != self.get_object().usuario:
                return self.handle_no_permission()

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['docPersonal'] = Documento.objects.filter(
            usuario=self.get_object().usuario
        )
        context['link_carpeta'] = obtenerFolderPorEmail(self.get_object().usuario.email)

        return context


class SolicitudUpdateView(LoginRequiredMixin,RolPermisosMixin, UpdateView):
    rol_permission_required = ('AS','SG')
    model = Solicitud
    fields = [
        #'problema', #No es necesario editar Problema y motivo
        #'motivo',
        'estado_solicitud',
        'expediente'
    ]
    template_name = 'ayuda_integral/solicitud/update_form.html'
    success_url = reverse_lazy('ayuda_integral:solicitud-list')
    
    def dispatch(self, request, *args, **kwargs):
        if self.get_object().estado_solicitud == Solicitud.AP or self.get_object().estado_solicitud == Solicitud.DE:
            return redirect('ayuda_integral:solicitud-detail', self.kwargs.get('pk'))
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['solicitud_contexto'] = self.get_object()
        return context

@login_required()
@rol_permission_required(('AS','SG'))
def delete_view(request, pk):
    solicitud = Solicitud.objects.get(pk=pk)
    solicitud.delete()
    return redirect('ayuda_integral:solicitud-list')


class SolicitudCreateView(LoginRequiredMixin,RolPermisosMixin, CreateView):
    rol_permission_required = ('AS','BE')
    form_class = SolicitudForm
    template_name = 'ayuda_integral/solicitud/create_form.html'
    success_url = reverse_lazy('ayuda_integral:solicitud-list')
    form_class = SolicitudForm


    def form_valid(self, form):
        # Guarda al beneficiario
        solicitud = form.save(commit=False)
        
        solicitud.beneficiario = None
        solicitud.usuario = self.request.user
        solicitud.save()

        return super().form_valid(form)
        
    def dispatch(self, request, *args, **kwargs):
        # usuario = self.request.user
        # APELLIDOS = usuario.apellidos
        # NOMBRES = usuario.nombres
        # print(APELLIDOS+ " "+ NOMBRES)
        # solicitante = api.obtenerBeneficiarios(APELLIDOS, NOMBRES,'alumnos')
        # if (not usuario.beneficiario):
        #     return redirect('ayuda_integral:solicitud-list')
        # beneficiario = Beneficiario.objects.filter(identificador = usuario.beneficiario.identificador).exists()
        # print(f"solicitante {len(solicitante)} ´{self.request.user.rol}" )
        # print(beneficiario)   
        # if(self.request.user.rol == 'BE' and len(solicitante) == 0):
        #     if(beneficiario):
        #         return super().dispatch(request, *args, **kwargs) 
        #     return redirect('ayuda_integral:solicitud-list')
        # return super().dispatch(request, *args, **kwargs)

        # if(self.request.user.rol == 'BE'):
        #    return redirect('ayuda_integral:solicitud-list')
        return super().dispatch(request, *args, **kwargs)

        
def beneficiario_create_view(request, pk):
    user = User.objects.get(pk = pk)
    NOMBRES = user.nombres
    APELLIDOS = user.apellidos

    if user.rol != 'BE':
        messages.success(
            request,
            'Solicitante no es un beneficiario',
            extra_tags='mensaje_solicitud',
        )
    else:
        solicitante = api.obtenerBeneficiarios(APELLIDOS, NOMBRES,'alumnos')
        if len(solicitante) != 0:
            print("alumno")
            solicitante = solicitante[0]
            CUI = solicitante['CUI']
            ESCUELA = solicitante['ESCUELA']
            alumno_registro(request, CUI, NOMBRES, APELLIDOS, ESCUELA,user)
        else:
            solicitante = api.obtenerBeneficiarios(APELLIDOS, NOMBRES,'docentes')
            if len(solicitante) != 0:
                print("docente")
                solicitante = solicitante[0]
                DNI = solicitante['DNI']
                ESCUELA = solicitante['ESCUELA']
                docente_registro(request, DNI, NOMBRES, APELLIDOS, ESCUELA,user)

            else:
                solicitante = api.obtenerBeneficiarios(APELLIDOS, NOMBRES,'administrativos')
                if len(solicitante) != 0:
                    print("administrativo")
                    solicitante = solicitante[0]
                    DNI = solicitante['DNI']
                    DEPENDENCIA = solicitante['DEPENDENCIA']
                    administrativo_registro(request, DNI, NOMBRES, APELLIDOS, DEPENDENCIA,user)

    return redirect(
        request.META.get('HTTP_REFERER')
    )

def  alumno_registro(request, ID, NOMBRES, APELLIDOS, ENTIDAD,user):    
    if(user.beneficiario):
        messages.error(
            request,
            'Beneficiario ya se encuentra registrado',
            extra_tags='mensaje_solicitud',
        )
    else:

        if(not (Beneficiario.objects.filter(identificador=ID).exists())):
            print(ENTIDAD)
            alumno = models.Beneficiario(
                is_interno = True,
                tipo_entidad = 'ES',
                tipo_rol = 'AL',
                nombres = NOMBRES,
                apellidos = APELLIDOS,
                identificador = ID,
                entidad = Entidad.objects.get(nombre = ENTIDAD)
            )
            alumno.save()
            user.beneficiario = alumno
            user.save()
            messages.success(
                request,
                'Beneficiario registrado exitosamente',
                extra_tags='mensaje_solicitud',
            )
        else:
            bene = Beneficiario.objects.get(identificador=ID)
            user.beneficiario = bene
            user.save()
            messages.success(
                request,
                'Beneficiario registrado exitosamente',
                extra_tags='mensaje_solicitud',
            )
    # crearEscuela()
    return 

def crearEscuela():
    bene = api.beneficiarios("2","alumnos")
    # print(len(bene))
    for ben in range(len(bene)):
        escuela = bene[ben]['ESCUELA']
        if(not (Entidad.objects.filter(nombre=escuela).exists())):
            entidad = models.Entidad(
                    nombre =escuela
                )
            entidad.save()
    return
def  docente_registro(request, DNI,  NOMBRES, APELLIDOS, ESCUELA, user):    
    if(not (Beneficiario.objects.filter(identificador=DNI).exists())):
        docente = models.Beneficiario(
            is_interno = True,
            tipo_entidad = 'ES',
            tipo_rol = 'PR',
            nombres = NOMBRES,
            apellidos = APELLIDOS,
            identificador = DNI,
            entidad = Entidad.objects.get(nombre = ESCUELA.upper())
        )
        docente.save()
        user.beneficiario = docente
        user.save()
        messages.success(
            request,
            'Beneficiario registrado exitosamente',
            extra_tags='mensaje_solicitud',
        )
    else:
        messages.error(
            request,
            'Beneficiario ya se encuentra registrado',
            extra_tags='mensaje_solicitud',
        )

def  administrativo_registro(request, DNI,  NOMBRES, APELLIDOS, DEPENDENCIA, user):    
    if(not (Beneficiario.objects.filter(identificador=DNI).exists())):
        administrativo = models.Beneficiario(
            is_interno = True,
            tipo_entidad = 'DP',
            tipo_rol = 'AD',
            nombres = NOMBRES,
            apellidos = APELLIDOS,
            identificador = DNI,
            entidad = Entidad.objects.get(nombre = DEPENDENCIA)
        )
        administrativo.save()
        user.beneficiario = administrativo
        user.save()
        messages.success(
            request,
            'Beneficiario ya se encuentra registrado',
            extra_tags='mensaje_solicitud',
        )
    else:
        messages.error(
            request,
            'Beneficiario ya se encuentra registrado',
            extra_tags='mensaje_solicitud',
        )

def exportar_reporteSolicitudes(request):
    return download()

def download():
    file_path = os.path.join(settings.BASE_DIR,"bienestar/ReporteSolicitudes.xlsx")
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
            return response

def obtenerFolderPorEmail(email):
    SCOPES = ['https://www.googleapis.com/auth/drive.readonly']
    KEY_FILE_LOCATION = os.path.join(settings.BASE_DIR,'bienestar/TramiteDocumentarioBienestar-c483db846e97.json')
    creds = ServiceAccountCredentials.from_json_keyfile_name(KEY_FILE_LOCATION, SCOPES)
    DRIVE = build('drive', 'v3', credentials=creds)

    response = DRIVE.files().list(q="mimeType = 'application/vnd.google-apps.folder' and '1BsY8Uytb_CqIgeRl-VraIGd8aYGeEq-m' in parents and name contains '(" + email + ")'",
                                            spaces='drive',
                                            fields='files(webViewLink)',
                                            ).execute()

    if response.get('files', []):
        archivo = response.get('files', [])[0]
        return archivo.get('webViewLink')
    else:
        return None
