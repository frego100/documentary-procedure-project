from django.urls import path
from ayuda_integral.views import documentos,beneficiarios

urlpatterns = [
    
    path('externos/', beneficiarios.SolicitantesExternosListView.as_view(),name='externo-list'),
    path('internos/', beneficiarios.SolicitantesInternosListView.as_view(),name='interno-list'),
    path('<int:pk>/document/',documentos.documento_list_view,name='documento-list'),
    path('<int:pk>/document/add',documentos.ExpedienteDocsBeneficiarioCreateView.as_view(),name='documento-create'),
    path('add/',beneficiarios.BeneficiarioExternoCreateView.as_view(),name='beneficiario-externo-create'),
    path('<int:pk>/',beneficiarios.BeneficiarioExternoDetailView.as_view(),name='beneficiario-externo-detail'),
    path('<int:pk>/update/',beneficiarios.BeneficiarioExternoUpdateView.as_view(),name='beneficiario-externo-update'),
    path('<int:pk>/delete/',beneficiarios.BeneficiarioExternoDeleteview.as_view(),name='beneficiario-externo-delete'),
    path('addInt/', beneficiarios.BeneficiarioInternoCreateView.as_view(),name='beneficiario-interno-create')
]