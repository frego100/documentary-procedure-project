from django.urls import path
from ayuda_integral.views import derivacion

urlpatterns = [
    path(
        'derivar',
        derivacion.derivacion_trabajador_view,
        name='derivar'
    ),
]