from django.urls import path
from ayuda_integral.views import notificaciones

urlpatterns = [
    path('<int:pk>/',notificaciones.NotificacionView.as_view(), name='redireccionar-de-notificacion'),
]