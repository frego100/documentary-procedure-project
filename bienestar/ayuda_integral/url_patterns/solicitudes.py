from django.urls import path
from ayuda_integral.views import solicitudes

urlpatterns = [
    path('',solicitudes.SolicitudListView.as_view(),name='solicitud-list'),
    path('add/',solicitudes.SolicitudCreateView.as_view(),name='solicitud-create'),
    path('<int:pk>/',solicitudes.SolicitudDetailView.as_view(),name='solicitud-detail'),
    path('beneficiario/add/<int:pk>/',solicitudes.beneficiario_create_view,name='beneficiario-add'),
    path('<int:pk>/update/',solicitudes.SolicitudUpdateView.as_view(),name='solicitud-update'),
    path('<int:pk>/delete/',solicitudes.delete_view,name='solicitud-delete'),
    path('exportarreporte/',solicitudes.exportar_reporteSolicitudes,name='exportar-reportesolicitudes')
]