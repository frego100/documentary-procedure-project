from django.urls import path
from ayuda_integral.views import entidad

urlpatterns = [
    path('',entidad.EntidadListView.as_view(),name='entidad-list'),
    path('add/',entidad.EntidadCreateView.as_view(),name='entidad-create'),
    path('<int:pk>/',entidad.EntidadDetailView.as_view(),name='entidad-detail'),
    path('<int:pk>/update/',entidad.EntidadUpdateView.as_view(),name='entidad-update'),
]