from django.urls import path
from ayuda_integral.views import apoyoPsico

urlpatterns = [
    path('', apoyoPsico.apoyoListView, name='apoyo-list'),
    path('buscar/',apoyoPsico.buscarSolicitudApoyoView,name="solicitud-search")
]