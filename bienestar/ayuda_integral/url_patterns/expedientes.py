from django.urls import path
from ayuda_integral.views import expedientes

urlpatterns = [
    path('',expedientes.ExpedienteListView.as_view(),name='expediente-list'),
    #path('', expedientes.ExpedienteReporteView.as_view(), name='expediente-list'),
# esta parte añadi un add sin nada para la creacion de un nuevo expediente
    path('add/',expedientes.ExpedienteCreateView.as_view(),name='expediente-add'),
# esta parte añadi un add con un parametro pk para que represente una nueva creacion 
# pero por parte de una solicitud, este pk guardara el id de la solicitud que pidio crear el expediente
# no se si es lo mas correcto, pero por el momento con eso funciona
    path('add/<int:pk>/',expedientes.ExpedienteCreateView.as_view(),name='expediente-add'),
    path('<int:pk>/',expedientes.ExpedienteDetailView.as_view(),name='expediente-detail'),
    path('<int:pk>/update/',expedientes.ExpedienteUpdateView.as_view(),name='expediente-update'),
    path('buscar/',expedientes.buscar_beneficiario_view,name="beneficiario-busqueda"),
    path('alumnos/',expedientes.alumnos_list_view,name="alumno-list"),
    path('docentes/',expedientes.docentes_list_view,name="docente-list"),
    path('administrativos/',expedientes.administrativos_list_view,name="administrativo-list"),
    path('<int:pk>/documentos/add/',expedientes.ExpedienteDocsCreateView.as_view(),name='expediente-doc-create'),
    path('<int:pk>/delete/',expedientes.delete_view,name='expediente-delete'),
    path('registrar/',expedientes.beneficiario_registro,name='beneficiario-registro'),
    path('exportar/',expedientes.exportar_reporte,name='exportar-reporte'),
    path('expedientedocumento/<pk>/delete/', expedientes.ExpedienteDocsDeleteView.as_view(), name='eliminar-exp-documento'), 

]