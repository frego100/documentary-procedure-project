from django.apps import AppConfig


class AyudaIntegralConfig(AppConfig):
    name = 'ayuda_integral'
