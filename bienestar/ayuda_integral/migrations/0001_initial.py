# Generated by Django 3.1.4 on 2020-12-15 02:42

from django.db import migrations
from main.models import User


def cargar_trabajadores(apps, schema_editor):
    UsuarioMiembro = apps.get_model('main', 'UsuarioMiembro')

    eliana_merma = UsuarioMiembro(
        correo='emermar@unsa.edu.pe',
        rol=User.SB,
        oficina=User.AI,
    )
    eliana_merma.save()
    maribel_alvarez = UsuarioMiembro(
        correo='malvarezbar@unsa.edu.pe',
        rol=User.JO,
        oficina=User.AI,
    )
    maribel_alvarez.save()
    secretaria = UsuarioMiembro(
        correo='ayudaintegral@unsa.edu.pe',
        rol=User.SO,
        oficina=User.AI,
    )
    secretaria.save()
    rosa_valdivia = UsuarioMiembro(
        correo='rvaldiviam@unsa.edu.pe',
        rol=User.TS,
        oficina=User.AI,
    )
    rosa_valdivia.save()
    elvia_mendoza = UsuarioMiembro(
        correo='emendozaz@unsa.edu.pe',
        rol=User.TS,
        oficina=User.AI,
    )
    elvia_mendoza.save()
    secretaria_general = UsuarioMiembro(
        correo='bienestar@unsa.edu.pe',
        rol=User.SG,
        oficina=User.AI,
    )
    secretaria_general.save()


class Migration(migrations.Migration):

    dependencies = [
        
    ]

    operations = [
        migrations.RunPython(cargar_trabajadores)
    ]
