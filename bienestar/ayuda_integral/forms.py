from django.forms import ModelForm
from main.models import Solicitud, Expediente, DocumentoExpediente, Documento, Beneficiario, Entidad


class SolicitudForm(ModelForm):
    class Meta:
        model = Solicitud
        fields = ['problema', 'motivo']
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        #poniendo los mensajes personalizados a cada atributo del modelo
        for field in self.fields.values():
            field.error_messages = {
                'required': 'Este campo es necesario',
                'invalid_choice' : 'Escoge una opción valida',
                'unique': 'Ya hay un registro con el mismo contenido',
                'max_length': 'El texto ingresado supera el limite',}

class ExpedienteForm(ModelForm):
    class Meta:
        model = Expediente
        fields = [
        'fecha_recepcion',
        'numero_expediente',
        'descripcion',
        'beneficiario',
        'numero_proveido',
        'numero_oficio',
        'actividad',
        'sub_actividad',
        ]
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        #poniendo los mensajes personalizados a cada atributo del modelo
        for field in self.fields.values():
            field.error_messages = {
                'required': 'Este campo es necesario',
                'invalid_choice' : 'Escoge una opción valida',
                'unique': 'Ya hay un registro con el mismo contenido',
                'max_length': 'El texto ingresado supera el limite',}

class ExpedienteUpdateForm(ModelForm):
    class Meta:
        model = Expediente
        fields = [
        'descripcion',
        'numero_proveido',
        'numero_oficio',
        'estado_expediente',
        'resultado'
        ]
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        #poniendo los mensajes personalizados a cada atributo del modelo
        for field in self.fields.values():
            field.error_messages = {
                'required': 'Este campo es necesario',
                'invalid_choice' : 'Escoge una opción valida',
                'unique': 'Ya hay un registro con el mismo contenido',
                'max_length': 'El texto ingresado supera el limite',}

class DocumentoExpedienteForm(ModelForm):
    class Meta:
        model = DocumentoExpediente
        fields = [
        'nombre',
        'path'
        ]
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        #poniendo los mensajes personalizados a cada atributo del modelo
        for field in self.fields.values():
            field.error_messages = {
                'required': 'Este campo es necesario',
                'invalid_choice' : 'Escoge una opción valida',
                'unique': 'Ya hay un registro con el mismo contenido',
                'max_length': 'El texto ingresado supera el limite',}

class DocumentoExpedienteBeneficiarioForm(ModelForm):
    class Meta:
        model = Documento
        fields = [
        'nombre',
        'path'
        ]
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        #poniendo los mensajes personalizados a cada atributo del modelo
        for field in self.fields.values():
            field.error_messages = {
                'required': 'Este campo es necesario',
                'invalid_choice' : 'Escoge una opción valida',
                'unique': 'Ya hay un registro con el mismo contenido',
                'max_length': 'El texto ingresado supera el limite',}

class BeneficiarioForm(ModelForm):
    class Meta:
        model = Beneficiario
        fields = [
        'nombres',
        'apellidos',
        'identificador',
        'entidad'
        ]
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        #poniendo los mensajes personalizados a cada atributo del modelo
        for field in self.fields.values():
            field.error_messages = {
                'required': 'Este campo es necesario',
                'invalid_choice' : 'Escoge una opción valida',
                'unique': 'Ya hay un registro con el mismo contenido',
                'max_length': 'El texto ingresado supera el limite',}

class BeneficiarioUpdateForm(ModelForm):
    class Meta:
        model = Beneficiario
        fields = [
        'nombres',
        'apellidos',
        'identificador',
        'entidad'
        ]
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        #poniendo los mensajes personalizados a cada atributo del modelo
        for field in self.fields.values():
            field.error_messages = {
                'required': 'Este campo es necesario',
                'invalid_choice' : 'Escoge una opción valida',
                'unique': 'Ya hay un registro con el mismo contenido',
                'max_length': 'El texto ingresado supera el limite',}

class EntidadForm(ModelForm):
    class Meta:
        model = Entidad
        fields = [
        'nombre',
        'descripcion',
        'area',
        ]
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        #poniendo los mensajes personalizados a cada atributo del modelo
        for field in self.fields.values():
            field.error_messages = {
                'required': 'Este campo es necesario',
                'invalid_choice' : 'Escoge una opción valida',
                'unique': 'Ya hay un registro con el mismo contenido',
                'max_length': 'El texto ingresado supera el limite',}

