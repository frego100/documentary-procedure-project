from django.urls import path, include
from django.views.generic import TemplateView
from ayuda_integral.url_patterns import expedientes, notificaciones
from django.contrib.auth.decorators import login_required, permission_required
from ayuda_integral.url_patterns import beneficiarios, solicitudes, derivacion, apoyoPsico, entidad


app_name = 'ayuda_integral'

urlpatterns = [
    path('',TemplateView.as_view(template_name='ayuda_integral/home.html'), name='home'),
    path('expedientes/', include(expedientes.urlpatterns)),
    path('expedientes/', include(derivacion.urlpatterns)),
    path('beneficiario/',include(beneficiarios.urlpatterns)),
    path('solicitud/',include(solicitudes.urlpatterns)),
    path('apoyoPsico/',include(apoyoPsico.urlpatterns)), 
    path('notificaciones/', include(notificaciones.urlpatterns)),
    path('entidad/', include(entidad.urlpatterns))
]
