from django.test import TestCase, Client
from main.models import User, Expediente, UsuarioDerivacion

class DerivacionTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.jefe = User.objects.create_user(
            email='secretaria@unsa.edu.pe',
            password='secretaria',
            rol=User.SG,
            oficina=User.AI,
        )
        cls.trabajadora = User.objects.create_user(
            email='trabajadora@unsa.edu.pe',
            password='trabajadora',
            rol=User.TS,
            oficina=User.AI
        )
    
    def setUp(self):
        self.client = Client()

    def test_derivacion_view(self):
        self.client.login(
            email='secretaria@unsa.edu.pe',
            password='secretaria'
        )

        expediente = Expediente.objects.create(
            numero_expediente='expediente-123',
        )

        usuario_a_derivar = User.objects.get(
            email='trabajadora@unsa.edu.pe'
        )

        self.client.post(
            '/ayuda-integral/expedientes/derivar',
            {
                'expedientes': [
                    expediente.pk,
                ],
                'usuarios': [
                    usuario_a_derivar.pk,
                ]
            }
        )
        
        self.assertQuerysetEqual(
            Expediente.objects.filter(
                derivados__usuario_derivado=usuario_a_derivar,
            ),
            [
                repr(expediente)
            ],
            msg='No se derivaron los expedientes al usuario'
        )