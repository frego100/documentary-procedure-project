# Proyecto de Trámite Documentario

Este proyecto tiene como objetivo crear un software capaz de hacer seguimiento de los reportes de la subdirección de logística de la UNSA.

# Instalación para el Desarrollo

## Requisitos
Se debe tener python 3.8
Se debe tener la herramienta pipenv

Una vez instalado pipenv, se debe usar el comando `pipenv install` para instalar las dependencias y `pipenv shell` para poder ejecutar el proyecto.

Se debe tener en cuenta que este repositorio no guarda las migraciones de django y la base de datos sqlite3. Para empezar probar el sistema de debe primero ir a la carpeta de bienestar con `cd bienestar` y ejecutar el comando `python manage.py makemigrations` y `python manage.py migrate` para generar las migraciones y la base de datos.

Para crear un usuario, se debe ejecutar los comandos `python manage.py loaddata demo` para tener datos de prueba en la base de datos y luego se debe usar el comando `python manage.py createsuperuser` y tendrá que poner '1' en el campo Oficina y tendrá que usar una nomenclatura para su rol (véase el archivo model.py de la carpeta main).

Para correr el servidor localmente se tendrá que usar el comando `python manage.py runserver`.
